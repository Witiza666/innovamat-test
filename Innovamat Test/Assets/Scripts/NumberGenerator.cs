﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

//Static class to write numbers with words in spanish. It can write from 0 to 9999
public static class NumberGenerator
{
    /*This functions translate a number from 0 to 9 to their corresponding word.*/
    #region DIGIT_FUNCTIONS
    static string thousands(int n)
    {
        switch (n)
        {
            case 0:
                return "";
            case 1:
                return "Mil ";
            case 2:
                return "Dos mil ";
            case 3:
                return "Tres mil ";
            case 4:
                return "Cuatro mil ";
            case 5:
                return "Cinco mil ";
            case 6:
                return "Seis mil ";
            case 7:
                return "Siete mil ";
            case 8:
                return "Ocho mil ";
            case 9:
                return "Nueve mil ";
        }
        return "ERROR";
    }

    static string hundreds(int n)
    {
        switch (n)
        {
            case 0:
                return "";
            case 1:
                return  "ciento ";
            case 2:
                return "doscientos ";
            case 3:
                return "trescientos ";
            case 4:
                return "cuatrocientos ";
            case 5:
                return "quinientos ";
            case 6:
                return "seiscientos ";
            case 7:
                return "setecientos ";
            case 8:
                return "ochocientos ";
            case 9:
                return "novecientos ";
        }
        return "ERROR";
    }
    static string tenths(int n)
    {
        switch (n)
        {
            case 0:
                return "";
            case 1:
                return "dieci";
            case 2:
                return "veinti";
            case 3:
                return "treinta y ";
            case 4:
                return "cuarenta y ";
            case 5:
                return "cincuenta y ";
            case 6:
                return "sesenta y ";
            case 7:
                return "setenta y ";
            case 8:
                return "ochenta y ";
            case 9:
                return "noventa y ";
        }
        return "ERROR";
    }
     static string tenthsWithZero(int n)
    {
        switch (n)
        {
            case 0:
                return "";
            case 1:
                return "diez";
            case 2:
                return "veinte";
            case 3:
                return "treinta";
            case 4:
                return "cuarenta";
            case 5:
                return "cincuenta";
            case 6:
                return "sesenta";
            case 7:
                return "setenta ";
            case 8:
                return "ochenta";
            case 9:
                return "noventa";
        }
        return "ERROR";
    }
    
    static string specialNumbers(int n)
    {
        switch (n)
        {
            case 0:
                return "Diez";
            case 1:
                return "Once";
            case 2:
                return "Doce";
            case 3:
                return "Trece";
            case 4:
                return "Catorze";
            case 5:
                return "Quince";
        }
        return "ERROR";
    }
    static string units(int n)
    {
        switch (n)
        {
            case 0:
                return "";
            case 1:
                return "uno ";
            case 2:
                return "dos ";
            case 3:
                return "tres ";
            case 4:
                return "cuatro ";
            case 5:
                return "cinco ";
            case 6:
                return "seis ";
            case 7:
                return "siete ";
            case 8:
                return "ocho ";
            case 9:
                return "nueve ";
        }
        return "ERROR";
    }
    #endregion
    /*Function that generates the number string from an integer*/
    public static string generateString(int number)
    {
        if(number<0||number>9999)
        {
            Debug.LogError("Incorrect number in the NumberGenerator script");
            return "ERROR";
        }

        string ret = "";
        string digits = number.ToString();

        //We reverse the string in order to get units, tenths, etc, always in the same position, not depending on the amount of digits
        digits = reverseString(digits);

        //We dont have corner cases in the thousands or hundreds so we are good to go.
        if (digits.Length >= 4)
            ret += thousands((int)char.GetNumericValue(digits[3]));
        if (digits.Length >= 3)
            ret += hundreds((int)char.GetNumericValue(digits[2]));

        //We need to cover two corner cases. numbers from 16-19, and numbers that have 0 units.
        if (digits.Length >= 2)
        {
            int ten = (int)char.GetNumericValue(digits[1]);
            int unit = (int)char.GetNumericValue(digits[0]);
            //We handle the first corner case here, as it wont enter if the number goes from 16 to 19.
            if (ten!=1 || unit > 5)
            {
                //Here we handle the second corner case. If the number has a 0 in units it will use another function instead.
                if (unit != 0)
                {
                    ret += tenths((int)char.GetNumericValue(digits[1]));
                    ret += units((int)char.GetNumericValue(digits[0]));
                }
                else
                {
                    ret += tenthsWithZero((int)char.GetNumericValue(digits[1]));
                }
            }
            else if(ten == 1 && unit < 5)
            {
                ret += specialNumbers(unit);
            }
        }
        //We capitalyze the first letter of the number.
        ret = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ret.ToLower());
        return ret;
    }

    //Returns the variable str as a completely reversed string
    public static string reverseString(string str)
    {
        char[] chars = str.ToCharArray();
        for (int i = 0, j = str.Length - 1; i < j; i++, j--)
        {
            chars[i] = str[j];
            chars[j] = str[i];
        }
        return new string(chars);
    }
}
