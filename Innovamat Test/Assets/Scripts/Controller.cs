﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum GameState
{
    ANNOUNCING,
    CHOOSING, 
};
public class Controller : MonoBehaviour
{
    [Range(0, 9999)]
    public int max_number = 10; //The maximum value of the number generated
    public int attempts = 2; //The amount of attempts the player has
    public float fade_time = 2.0f; //The animation time
    [Header("Colors")]
    public Color correct_color = Color.green; //Color of the button when the answer is correct
    public Color incorrect_color = Color.red; //Color of the button when the answer is not correct
    public Color default_color = Color.white; //Default color of the button
    [Header("Scene Objects")]
    public GameObject[] options; //Possible options that the player has
    public Text announcer; //Text with the solution in text
    public Text sucesses; //Text with the amount of correct answers
    public Text failures; //Text with the amount of incorrect answers

    float current_fade = 0.0f; //Current value of the fade, going from 0 to fade_time
    int current_attempts = 0; //Current amount of attempts, reset each loop
    int current_answer; //Current correct answer for the loop
    int answer_index; //Current correct answer in the options[] array

    Dictionary<int,string> numbers = new Dictionary<int, string>(); //Dictionary with the names for each number. Generated at the start of the scene
    GameState state = GameState.ANNOUNCING; //Current game state
    int sucess_counter = 0; //Amount of successes
    int failure_counter = 0; //Amount of failures

    //Booleans to indicate if the numbers are currently fading
    bool fading_in = false;
    bool fading_out = false;
   

    // Start is called before the first frame update
    void Start()
    {
        //We fill the dictionary
        for(int i = 0;i<=max_number;i++)
        {
            numbers.Add(i, NumberGenerator.generateString(i));
        }
        //Generate the first problem
        Initialyze();
    }

    //Function that starts a new loop
    void Initialyze()
    {
        //Fill the announcer and options
        generateProblem();
        //Disable buttons
        changeUIinteractability(false);
        //start gade
        fading_in = true;
        current_fade = 0.0f;
        //Reset state
        state = GameState.ANNOUNCING;
        //Reset colors
        assignColors(default_color);
        //Hide buttons
        assignAlpha(0);
    }
    // Update is called once per frame
    void Update()
    {
        //We check which state we are in
        if(state == GameState.ANNOUNCING)
        {
            //In the announcing state, we are constantly "fading"
            Color color = announcer.color;
            current_fade += Time.deltaTime;
            if (fading_in)
            {
                color.a = Mathf.Lerp(0, 1, current_fade / fade_time);
                if(current_fade >= fade_time)
                {
                    fading_in = false;
                    current_fade = 0.0f;
                }
            }
            else if(fading_out)
            {
                color.a = Mathf.Lerp(1, 0, current_fade / fade_time);
                if (current_fade >= fade_time)
                {
                    //Fading out in the announcing state means that we transition to the next state
                    fading_out = false;
                    current_fade = 0.0f;
                    state = GameState.CHOOSING;
                    color.a = 0.0f;
                    fading_in = true;
                }
            }
            else
            {
                //We use the fade time and current fade as timers for the two seconds the announcement is in the screen
                if (current_fade >= fade_time)
                {
                    fading_out = true;
                    current_fade = 0.0f;
                }
            }
            announcer.color = color;
        }
        else if(state==GameState.CHOOSING)
        {
            //We assume that all the options have the same alpha
            float alpha = options[0].GetComponentInChildren<Text>().color.a;
            if (fading_in)
            {
                current_fade += Time.deltaTime;
                alpha = Mathf.Lerp(0, 1, current_fade / fade_time);
                assignAlpha(alpha);
                if (current_fade >= fade_time)
                {
                    //Once we have fully faded in, we activate the UI and let the player choose.
                    fading_in = false;
                    current_fade = 0.0f;
                    changeUIinteractability(true);
                }

            }
            else if(fading_out)
            {
                current_fade += Time.deltaTime;
               alpha = Mathf.Lerp(1, 0, current_fade / fade_time);
                assignAlpha(alpha);

                if (current_fade >= fade_time)
                {
                    //If the fade out of the CHOOSING state has finished, means that the user has either suceeded or failed all the attempts, so the game resets.
                    Initialyze();
                }
            }
        }
    }

    //We generate the numbers for the problem
    void generateProblem()
    {
        current_attempts = 0;
        //Generate both the correct number and the option that is going to have it.
        current_answer = Random.Range(0, max_number+1);
        answer_index = Random.Range(0, options.Length);
        //We set both the announcer and the correct option
        assignNumberToText(announcer,current_answer);
        options[answer_index].GetComponentInChildren<Text>().text = current_answer.ToString();

        //We fill all the options aside from the correct one
        for(int i = 0;i<options.Length;i++)
        {
            if(i != answer_index)
            {
                int number = 0;
                //Corner case where the random number generated matches the correct answer
                do {
                     number = Random.Range(0, max_number + 1);
                } while (number == current_answer);
                options[i].GetComponentInChildren<Text>().text = number.ToString();
            }
        }
    }
    //We get our text from the dictionary and assign it to our UI text
    bool assignNumberToText(Text text,int number)
    {
        string result;
        if (numbers.TryGetValue(number, out result))
        {
            text.text = result;
            return true;
        }
        else
        {
            Debug.LogError("Error in the dictionary, could not find " + current_answer);
            return false;
        }
    }
    //Callback for each button
    public void buttonPressed(GameObject go)
    {
        //We find which option has been pressed
        int index = -1;
        for (int i = 0; i < options.Length; i++)
        {
            if(go == options[i])
            {
                index = i;
                break;
            }
        }
        //Check for unassigned buttons
        if(index == -1)
        {
            Debug.LogError("Option button not assigned in the controller");
        }
        //We check if the answer was the correct one
        else if(index == answer_index)
        {
            //We update the UI
            go.GetComponentInChildren<Image>().color = correct_color;
            sucess_counter++;
            sucesses.text = "Aciertos: " + sucess_counter;
            //Start the fade out in order to reset
            fading_out = true;
            changeUIinteractability(false);
        }
        else
        {
            //Update the UI
            go.GetComponentInChildren<Image>().color = incorrect_color;
            failure_counter++;
            failures.text = "Fallos: " + failure_counter;
            current_attempts++;
            //We disable the pressed button
            go.GetComponentInChildren<Button>().interactable = false;
            //If the player reaches the maximum amount of attempts, we start the fade out to reset
            if(current_attempts>=attempts)
            {
                fading_out = true;
                changeUIinteractability(false);
            }
        }

    }

    //Change the alpha of both the button image and the button text of each option
    void assignAlpha(float alpha)
    {
        Color color;
        for (int i = 0; i < options.Length; i++)
        {
            color = options[i].GetComponentInChildren<Text>().color;
            color.a = alpha;
            options[i].GetComponentInChildren<Text>().color = color;

            color = options[i].GetComponentInChildren<Image>().color;
            color.a = alpha;
            options[i].GetComponentInChildren<Image>().color = color;
        }
    }
    //Disable/Enable buttons
    void changeUIinteractability(bool active)
    {
        for(int i = 0;i<options.Length;i++)
        {
            options[i].GetComponentInChildren<Button>().interactable = active;
        }
    }
    //Assign colors to the options
    void assignColors(Color color)
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponentInChildren<Image>().color = color;
        }
    }

}
